"enable mouse
if has("mouse_sgr")
  set ttymouse=sgr
  set mouse=a
else
  set ttymouse=xterm2
  set mouse=a
end

if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

"special
set nocompatible
set shell=/bin/sh
set t_Co=256
"set termguicolors
set ttimeoutlen=50
set clipboard=unnamedplus
setlocal iskeyword+=-
augroup CursorLine
    au!
    au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
    au WinLeave * setlocal nocursorline
augroup END

"add comment on new line
set formatoptions=tcroql

"interface
"set scrolloff=999
set number
set cursorline
set nowrap
set wildmenu
"set spell
set ignorecase
set equalalways
set winminheight=0
set winminwidth=0
set pastetoggle=<F10>
set laststatus=2
set completeopt=longest,menuone
set backspace=indent,eol,start
let NERDTreeWinSize=16
set colorcolumn=80
set incsearch
set noshowmode

"tabs
set tabstop=2
set shiftwidth=2
set expandtab
set smartindent
set smarttab
set autoindent
"set breakindent

"persistent undo and backups
set undofile
set undodir=~/.vim/undodir
set backupdir=~/.vim/backup
set directory=~/.vim/swp

"improve performance over SSH
set lazyredraw
set ttyfast

"set cursorline
autocmd FileType qf wincmd J

"mappings
au InsertLeave go GoFmt
au FileType go nmap ' <Plug>(go-run)
au FileType go setlocal tabstop=8
" au FileType go nmap <enter> :GoDoc<enter>
au FileType go nmap \<enter> <Plug>(go-def-vertical)
au FileType go nmap " :GoMetaLinter<enter>
map <C-q> :b#<bar>bd#<CR>
nnoremap k gk
nnoremap K 5k
nnoremap j gj
nnoremap J 5j
nmap <C-=> ggvG=<C-o><C-o> 
nmap a A
au FileType *.tmpl nmap + GGVG=<C-o> 
nmap Z :GoFmt<enter>
nmap <C-d> <esc>:qa<enter>
nmap ^ :GoDescribe<enter>
nmap - o<Esc>
nmap ; :w<enter>
nmap <space> :
nmap <Tab> <C-w>w
nmap <S-Tab> <C-w>W
nmap <leader>e xierr<esc>oif err != nil {<enter>
map <S-F9> :TagbarToggle<enter>
nmap \| :TagbarToggle<enter>
map <F9> :NERDTreeToggle<enter>
nmap <leader>u :UndotreeToggle<enter>
nmap \\ :NERDTreeToggle<enter>
map <S-F10> :setlocal spell! spelllang=en_us<CR>
map <S-F5> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

"plugins
call plug#begin('~/.vim/plugged')
  Plug 'scrooloose/nerdtree'
  " Plug 'jistr/vim-nerdtree-tabs'
  Plug 'majutsushi/tagbar'
  Plug 'mbbill/undotree'
  " Plug 'rafi/vim-tinyline'
  Plug 'w0rp/ale'
  Plug 'ctrlpvim/ctrlp.vim'
  Plug 'francoiscabrol/ranger.vim'
  Plug 'Xuyuanp/nerdtree-git-plugin'

  Plug 'lifepillar/vim-mucomplete'
  Plug 'jiangmiao/auto-pairs'
  " Plug 'ervandew/supertab'
  " Plug 'Shougo/echodoc.vim'
  " Plug 'SirVer/ultisnips'
    
  Plug 'tpope/vim-commentary'
  Plug 'tpope/vim-fugitive'

  Plug 'fatih/vim-go'

  Plug 'morhetz/gruvbox'

  Plug 'vim-scripts/c.vim'
  Plug 'Rip-Rip/clang_complete'

  Plug 'alvan/vim-closetag'
  Plug 'ap/vim-css-color'
  Plug 'posva/vim-vue'
  Plug 'jelera/vim-javascript-syntax'
  Plug 'jiangmiao/simple-javascript-indenter'
  Plug 'mxw/vim-jsx'
  Plug 'pangloss/vim-javascript'
  Plug 'maksimr/vim-jsbeautify'
call plug#end()

filetype plugin indent on

" ctrlp
" let g:ctrlp_custom_ignore = 'node_modules\|git\BACKEDUP'
" let g:ctrlp_max_files=0

let g:clang_library_path='/usr/lib64/libclang.so.3.9'
au Filetype c let b:autopairs_loaded=1

"GO
" set updatetime=100
" let g:go_auto_sameids = 1
let g:go_highlight_operators = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_array_whitespace_error = 1
let g:go_highlight_chan_whitespace_error = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_space_tab_error = 1
let g:go_highlight_trailing_whitespace_error = 1
let g:completor_go_omni_trigger = '(?:\b[^\W\d]\w*|[\]\)])\.(?:[^\W\d]\w*)?'
let g:go_list_type = "quickfix"
let g:go_fmt_command = "goimports"
let g:go_auto_type_info = 1

"NERDTree
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

"mucomplete
set shortmess+=c
set completeopt-=preview
" set completeopt+=longest,menu,menuone,noinsert,noselect
set completeopt+=menuone,noinsert
let g:mucomplete#enable_auto_at_startup = 1
let g:mucomplete#can_complete = {
  \ 'default': {
  \    'omni': { t -> strlen(&l:omnifunc) > 0 && t =~# '\%(\k\|\.\)$' }
  \    }
  \  }

" colorscheme gruvbox
" set background=dark    " Setting dark mode
" let g:gruvbox_contrast_dark = "medium"
" set background=light" Setting dark mode
" let g:gruvbox_contrast_light = "hard"
"line num color
" let g:ctrlp_working_path_mode = 'ra'
set wildignore+=*/tmp/*,*/BACKEDUP/*,*/Downloads/*,*/.mozilla/*,*.so,*.swp,*.zip     " Linux/MacOSX
autocmd InsertEnter * hi CursorLineNR ctermfg=022 ctermbg=148 cterm=bold
autocmd InsertLeave * hi CursorLineNR ctermfg=255 ctermbg=red cterm=none
"Custom syntax highlighting
hi Boolean ctermfg=089
hi ColorColumn ctermbg=229
hi Comment ctermfg=065
hi CursorLine cterm=NONE ctermbg=228
hi CursorLineNR ctermfg=255 ctermbg=red
hi Function ctermfg=029
hi GoEscapeC cterm=bold ctermfg=291
hi GoSpace ctermbg=222
hi GoTodo ctermbg=214 ctermfg=232
hi GoTypeConstructor ctermfg=161
hi GoField ctermfg=red
hi Keyword ctermfg=124
hi LineNr ctermfg=016 ctermbg=220 
hi MatchParen cterm=bold ctermbg=085 ctermfg=016
hi Method ctermfg=106 ctermbg=085
hi Number ctermfg=172
hi Pmenu ctermbg=148 ctermfg=064
hi PmenuSel ctermfg=058 ctermbg=142
hi Search ctermbg=226
hi SignColumn ctermbg=016
hi spellbad ctermbg=224 ctermfg=196
hi StatusLineNC ctermfg=220 ctermbg=016
hi VertSplit ctermfg=232 ctermbg=255
hi Visual ctermbg=226

" let g:ale_linters = {
" \   'go': ['gofmt'],
" \}

" jsbeautify
map <c-f> :call JsBeautify()<cr>
